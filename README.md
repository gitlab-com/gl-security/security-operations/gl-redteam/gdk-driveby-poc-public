# gdk-driveby-poc-public

This project contains the exploit source code from the original GitLab GDK drive-by RCE disclosure. You should not deploy this code in any Internet-accessible locations. It is being provided for research purposes only.

You can read more about this in the blog [here](https://about.gitlab.com/blog/2021/09/07/why-are-developers-vulnerable-to-driveby-attacks/).

Vulnerability research by [Chris Moberly](https://gitlab.com/cmoberly), exploit development by [Chris Moberly](https://gitlab.com/cmoberly) and [Frederic Loudet](https://gitlab.com/floudet).
